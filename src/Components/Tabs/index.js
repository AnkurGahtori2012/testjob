import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import { Tab } from 'react-bootstrap';
import PropTypes, { array } from 'prop-types';

const CustomTabs = ({ tabList, activeTab }) => console.log('acitve tab, ', activeTab) || (
  <Tabs defaultActiveKey={activeTab} transition={false} id="noanim-tab-example" style={{
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  }}
  className="nav nav-justified">
    {tabList.map(({ Component, title, id, disabled }) => (
      <Tab key={id} eventKey={title} title={title} disabled={disabled} >
        <Component />
      </Tab>
    ))}
  </Tabs>
);

CustomTabs.defaultProps = {
  tabList: [],
  activeTab: '',
};

CustomTabs.propTypes = {
  tabList: PropTypes.instanceOf(array),
  activeTab: PropTypes.string,
};

export default CustomTabs;
