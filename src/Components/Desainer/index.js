import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button, InputGroup, FormControl } from 'react-bootstrap';
import { fetchUsers } from "../../redux/actions";
import Card from "../Card";

const Desainer = () => {
  const dispatch = useDispatch();
  const Users = useSelector((state) => {
    return state?.usersData ?? [];
  });

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  return (
    <div className="card-container">
      <InputGroup className="mb-3">
        <FormControl
          placeholder="Cari Nana Peserta"
          aria-label="Cari Nana Peserta"
          aria-describedby="basic-addon1"
          className="search"
        />
      </InputGroup>
      {Users.map((user) => (
        <Card key={user._id} userInfo={user} />
      ))}
      <Button variant="primary" className="button">Undang Bergabug</Button>
    </div>
  );
};
export default Desainer;
