import React from 'react';
import Card from 'react-bootstrap/Card';
import { StarFill, TrophyFill, Dot } from 'react-bootstrap-icons';

const ProfileCard = ({ userInfo }) => (
  <Card>
    <Card.Body>
      <div className="row-details ">
        <div className="img-wrapper"><Card.Img className="rounded-circle" src={userInfo.picture} width="80px" /></div>
        <div className="basic-info">
          <div className="row-name">
            <div className="name">{userInfo.name}</div>
            <div className="company-name">
              <span>{`Desianer ${userInfo.company}`}</span>
              <span className="dot">
                <Dot size={25} color="#636363" />
                {userInfo.tags.join(' & ')}
              </span>
            </div>
          </div>
          <div className="badges">
            <div className="star">
              <StarFill color="#E3B26A" size="20" />
              <div className="count">4.9</div>
            </div>
            <div className="star">
              <TrophyFill color="#E3B26A" size="20" />
              <div className="count">25</div>
            </div>
          </div>
        </div>
      </div>
    </Card.Body>

  </Card>

);

export default ProfileCard;
