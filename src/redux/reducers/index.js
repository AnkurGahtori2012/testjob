import {
  FETCH_USER_LOADING,
  FETCH_USER_FULFILLED,
  FETCH_USER_REJECTED,
} from "../constants";
export const initialState = {
  usersDataLoading: false,
  usersData: [],
  error: "",
};
export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_USER_LOADING:
      return {
        ...state,
        usersDataLoading: true,
      };
    case FETCH_USER_FULFILLED:
      return {
        ...state,
        usersDataLoading: false,
        usersData: action.payload,
      };
    case FETCH_USER_REJECTED:
      return {
        ...state,
        usersDataLoading: false,
        error: action.payload?.message ?? "Something went wrong",
      };
    default:
      return state;
  }
};
