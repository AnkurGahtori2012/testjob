import axios from "axios";
import * as types from '../constants';

export const fetchUserData = () => ({
  type: types.FETCH_USER_LOADING,
});

export const fetchUserDataFulfilled = (response) => ({
  type: types.FETCH_USER_FULFILLED,
  payload: response
});

export const fetchUserDataFailed = (response) => ({
  type: types.FETCH_USER_REJECTED,
  payload: response
});

export const fetchUsers = () => async dispatch => {
  try {
    dispatch(fetchUserData());
    const url = "https://iunaptk810.execute-api.ap-southeast-1.amazonaws.com/dev/api/users";
    const response = await axios.get(url);
    dispatch(fetchUserDataFulfilled(response?.data ?? []));
  } catch (err) {
    dispatch(fetchUserDataFailed(err));
  }
};