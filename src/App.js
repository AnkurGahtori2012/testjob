import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

import Tabs from "./Components/Tabs";
import Desainer from "./Components/Desainer";
import "./App.scss";
import Header from "./Components/Header";
import { fetchUsers } from "./redux/actions";

const tabList = [
  {
    title: "Desainer",
    Component: Desainer,
    id: "desainer",
    disabled: false
  },
  {
    title: "Guru",
    Component: () => "guru",
    id: "guru",
    disabled: true
  },
];

function App() {
  const dispatch = useDispatch();
  const Users = useSelector((state) => {
    return state?.usersData ?? [];
  });
  const [show, setShow] = useState(false);
  const [activeTab] = useState("Desainer");
  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  console.log(Users);
  return (
    <div
      className="d-flex justify-content-center align-items-center"
      style={{ height: "100vh" }}
    >
      <Button className="p-2" variant="primary" onClick={handleShow}>
        Modal
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            <Header />
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tabs tabList={tabList} activeTab={activeTab} />
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default App;
